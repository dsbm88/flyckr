package com.example.dmartins.flickr.models;

/**
 * Created by E491 on 23/11/2016.
 */

public class PhotoResponse {

    private Photos photos;

    public Photos getPhotos() {
        return photos;
    }

    public void setPhotos(Photos photos) {
        this.photos = photos;
    }
}
