package com.example.dmartins.flickr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.dmartins.flickr.R;
import com.example.dmartins.flickr.models.Photo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by E491 on 23/11/2016.
 */

public class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 1;
    private final int VIEW_PROGRESS_ITEM = 2;
    private boolean isFooterEnabled = false;

    private Context mContext;
    private List<Photo> mListPhoto;

    public GalleryAdapter(Context mContext, List<Photo> mListPhoto) {
        this.mContext = mContext;
        this.mListPhoto = mListPhoto;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_item, parent, false);
            vh = new PictureViewHolder(view);
        } else if (viewType == VIEW_PROGRESS_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_bar_item, parent, false);
            vh = new ProgressViewHolder(view);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder)holder).progressBar.setIndeterminate(true);
        }
        else if (mListPhoto.size() > 0 && position < mListPhoto.size()) {
            final Photo photo = mListPhoto.get(position);
            Picasso.with(mContext)
                    .load(photo.getUrl())
                    .error(R.drawable.error)
                    .into(((PictureViewHolder) holder).photoImg);
        }

    }

    @Override
    public int getItemCount() {
        return (isFooterEnabled) ? mListPhoto.size() + 1 : mListPhoto.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (isFooterEnabled && position >= mListPhoto.size() ) ? VIEW_PROGRESS_ITEM : VIEW_TYPE_ITEM;
    }

    public class PictureViewHolder extends RecyclerView.ViewHolder {
        ImageView photoImg;
        public PictureViewHolder(View itemView) {
            super(itemView);
            photoImg = (ImageView) itemView.findViewById(R.id.photo_item);
        }
    }


    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;
        public ProgressViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }



    public void addAllPhotos(ArrayList<Photo> photos) {
        mListPhoto.addAll(photos);
    }

}
