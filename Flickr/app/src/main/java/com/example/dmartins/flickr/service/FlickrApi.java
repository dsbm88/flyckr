package com.example.dmartins.flickr.service;

import com.example.dmartins.flickr.models.PhotoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by E491 on 23/11/2016.
 */

public interface FlickrApi {

    String URL = "https://api.flickr.com/services/";
    String KEY = "cf2a63a4af252b0262a8fab86764790b";
    String METHOD = "flickr.photos.getRecent";
    String JSON = "json";
    String NO_JSON_CALLBACK = "1";


    @GET("rest/")
    Call<PhotoResponse> getRecentPhotos(@Query("method") String method,
                                        @Query("api_key") String apiKey,
                                        @Query("format") String format,
                                        @Query("nojsoncallback") String noJsonCallback);

    @GET("rest/")
    Observable<PhotoResponse> getObservableRecentPhotos(@Query("method") String method,
                                                        @Query("api_key") String apiKey,
                                                        @Query("format") String format,
                                                        @Query("nojsoncallback") String noJsonCallback);

}
