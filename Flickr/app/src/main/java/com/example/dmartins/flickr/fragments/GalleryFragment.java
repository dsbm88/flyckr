package com.example.dmartins.flickr.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.dmartins.flickr.R;
import com.example.dmartins.flickr.adapter.GalleryAdapter;
import com.example.dmartins.flickr.models.Photo;
import com.example.dmartins.flickr.models.PhotoResponse;
import com.example.dmartins.flickr.service.FlickrApi;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by E491 on 23/11/2016.
 */
public class GalleryFragment extends Fragment {

    RecyclerView mRecyclerView;
    GalleryAdapter mGalleryAdapter;
    Button clickButton;
    Subscription mSubscription;
    ArrayList<Photo> list = new ArrayList<>();


    public GalleryFragment() {
    }

    public static GalleryFragment newInstance() {
        return new GalleryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.photos_recycler_view);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        mGalleryAdapter = new GalleryAdapter(getActivity(), new ArrayList<Photo>());
        mRecyclerView.setAdapter(mGalleryAdapter);


        clickButton = (Button) rootView.findViewById(R.id.click);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhotos();
            }
        });


        return rootView;
    }

    /**
     * Using only retrofit to make http callback
     */
    private void getPhotos() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FlickrApi.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        FlickrApi flickrService = retrofit.create(FlickrApi.class);

        Call<PhotoResponse> requestRecentPhotos = flickrService.getRecentPhotos(
                FlickrApi.METHOD,
                FlickrApi.KEY,
                FlickrApi.JSON,
                FlickrApi.NO_JSON_CALLBACK);

        requestRecentPhotos.enqueue(new Callback<PhotoResponse>() {
            @Override
            public void onResponse(Call<PhotoResponse> call, Response<PhotoResponse> response) {
                ArrayList<Photo> listOfPhotos = new ArrayList<>();
                if (response.isSuccessful()) {
                    for (Photo p : response.body().getPhotos().getPhoto()) {
                        Photo photo = new Photo(
                                p.getId(),
                                p.getSecret(),
                                p.getServer(),
                                p.getFarm()
                        );
                        listOfPhotos.add(photo);
                    }
                }
                mGalleryAdapter.addAllPhotos(listOfPhotos);
                mGalleryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<PhotoResponse> call, Throwable t) {
                Log.d("TESTE", t.getMessage());
            }
        });


    }

    /**
     * Using RXJava and Retrofit to make http asynchronously
     */
    private void getPhotosUsingRxJava() {
        Retrofit retrofit2 = new Retrofit.Builder()
                .baseUrl(FlickrApi.URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        FlickrApi observableFlickrService = retrofit2.create(FlickrApi.class);


        Observable<PhotoResponse> observableRequestRecentPhotos = observableFlickrService.getObservableRecentPhotos(
                FlickrApi.METHOD,
                FlickrApi.KEY,
                FlickrApi.JSON,
                FlickrApi.NO_JSON_CALLBACK
        );

        mSubscription = observableRequestRecentPhotos
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PhotoResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(PhotoResponse photoResponse) {
                        for (Photo p : photoResponse.getPhotos().getPhoto()) {
                            Photo photo = new Photo(
                                    p.getId(),
                                    p.getSecret(),
                                    p.getServer(),
                                    p.getFarm()
                            );
                            list.add(photo);
                        }
                        mGalleryAdapter.addAllPhotos(list);
                        mGalleryAdapter.notifyDataSetChanged();
                    }
                });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }
}
