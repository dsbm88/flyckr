package com.example.dmartins.flickr.activities;

import android.os.Bundle;

import com.example.dmartins.flickr.fragments.GalleryFragment;
import com.example.dmartins.flickr.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.container, GalleryFragment.newInstance())
                .commit();
    }
}
